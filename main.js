
class Employee {
    constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary;
    }
  
    
    getName() {
      return this.name;
    }
  
    setName(name) {
      this.name = name;
    }
  
    getAge() {
      return this.age;
    }
  
    setAge(age) {
      this.age = age;
    }
  
    getSalary() {
      return this.salary;
    }
  
    setSalary(salary) {
      this.salary = salary;
    }
  }
  
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    
    getSalary() {
      return this.salary * 3;
    }
  }
  
  
  const programmer1 = new Programmer('John', 30, 5000, ['JavaScript', 'Python']);
  const programmer2 = new Programmer('Jane', 35, 6000, ['Java', 'C++']);
  
 
  console.log(programmer1.getName());     
  console.log(programmer1.getAge());      
  console.log(programmer1.getSalary());   
  console.log(programmer1.lang);          
  
  console.log(programmer2.getName());     
  console.log(programmer2.getAge());     
  console.log(programmer2.getSalary());   
  console.log(programmer2.lang);          
  